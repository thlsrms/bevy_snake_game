use crate::food::FoodSpawnEvent;
use crate::transform::{Position, Size};
use crate::{GameOverEvent, ARENA_HEIGHT, ARENA_WIDTH};
use bevy::prelude::*;

const SNAKE_HEAD_COLOR: Color = Color::rgb(0.7, 0.65, 0.0);
const SNAKE_SEGMENT_COLOR: Color = Color::rgb(0.75, 0.3, 0.03);

#[derive(Component)]
pub struct SnakeHead {
    direction: MovementDirection,
    last_movement_pos: Position, // To avoid two direction changes at the same location
}
#[derive(Component)]
pub struct SnakeSegment;
#[derive(Resource, Default, Deref, DerefMut)]
pub struct SnakeSegments(Vec<Entity>);
#[derive(Resource, Default)]
pub struct LastTailPosition(Option<Position>);
pub struct GrowthEvent;

#[derive(PartialEq, Copy, Clone)]
enum MovementDirection {
    Left,
    Up,
    Right,
    Down,
}

impl MovementDirection {
    fn opposite(self) -> Self {
        match self {
            Self::Up => Self::Down,
            Self::Down => Self::Up,
            Self::Left => Self::Right,
            Self::Right => Self::Left,
        }
    }
}

pub fn spawn(
    mut commands: Commands,
    mut segments: ResMut<SnakeSegments>,
    mut food_spawn_writer: EventWriter<FoodSpawnEvent>,
) {
    *segments = SnakeSegments(vec![
        commands
            .spawn(SpriteBundle {
                sprite: Sprite {
                    color: SNAKE_HEAD_COLOR,
                    ..default()
                },
                ..default()
            })
            .insert(SnakeHead {
                direction: MovementDirection::Up,
                last_movement_pos: Position { x: 3, y: 3 },
            })
            .insert(Position { x: 3, y: 3 })
            .insert(Size::square(0.8))
            .insert(SnakeSegment)
            .id(),
        create_segment(commands, Position { x: 3, y: 3 }),
    ]);
    food_spawn_writer.send(FoodSpawnEvent);
}

pub fn movement(
    segments: ResMut<SnakeSegments>,
    mut last_tail_position: ResMut<LastTailPosition>,
    mut game_over_writter: EventWriter<GameOverEvent>,
    mut heads: Query<(Entity, &SnakeHead)>,
    mut positions: Query<&mut Position>,
) {
    if let Some((head_entity, head)) = heads.iter_mut().next() {
        let segment_positions = segments
            .iter()
            .map(|e| *positions.get_mut(*e).unwrap())
            .collect::<Vec<Position>>();
        let mut head_pos = positions.get_mut(head_entity).unwrap();

        match &head.direction {
            MovementDirection::Left => head_pos.x -= 1,
            MovementDirection::Right => head_pos.x += 1,
            MovementDirection::Down => head_pos.y -= 1,
            MovementDirection::Up => head_pos.y += 1,
        }
        if head_pos.x < 0
            || head_pos.y < 0
            || head_pos.x as u32 >= ARENA_WIDTH
            || head_pos.y as u32 >= ARENA_HEIGHT
        {
            game_over_writter.send(GameOverEvent);
        }
        if segment_positions.contains(&head_pos) {
            game_over_writter.send(GameOverEvent);
        }

        segment_positions
            .iter()
            .zip(segments.iter().skip(1))
            .for_each(|(pos, segment)| {
                *positions.get_mut(*segment).unwrap() = *pos;
            });
        *last_tail_position = LastTailPosition(Some(*segment_positions.last().unwrap()));
    }
}

pub fn movement_input(
    keyboard_input: Res<Input<KeyCode>>,
    mut heads: Query<(&mut SnakeHead, &Position)>,
) {
    if let Some((mut head, pos)) = heads.iter_mut().next() {
        let dir: Option<MovementDirection> = if keyboard_input.pressed(KeyCode::Left) {
            Some(MovementDirection::Left)
        } else if keyboard_input.pressed(KeyCode::Down) {
            Some(MovementDirection::Down)
        } else if keyboard_input.pressed(KeyCode::Up) {
            Some(MovementDirection::Up)
        } else if keyboard_input.pressed(KeyCode::Right) {
            Some(MovementDirection::Right)
        } else {
            None
        };
        if let Some(dir) = dir {
            if dir != head.direction.opposite()
                && (pos.x != head.last_movement_pos.x || pos.y != head.last_movement_pos.y)
            {
                head.direction = dir;
                head.last_movement_pos = *pos;
            }
        }
    }
}

fn create_segment(mut commands: Commands, position: Position) -> Entity {
    commands
        .spawn(SpriteBundle {
            sprite: Sprite {
                color: SNAKE_SEGMENT_COLOR,
                ..default()
            },
            ..default()
        })
        .insert(SnakeSegment)
        .insert(position)
        .insert(Size::square(0.65))
        .id()
}

pub fn eating(
    mut commands: Commands,
    mut growth_writer: EventWriter<GrowthEvent>,
    mut food_spawn_writer: EventWriter<FoodSpawnEvent>,
    food_positions: Query<(Entity, &Position), With<crate::food::Food>>,
    head_positions: Query<&Position, With<SnakeHead>>,
) {
    for head_pos in head_positions.iter() {
        for (ent, food_pos) in food_positions.iter() {
            if food_pos == head_pos {
                commands.entity(ent).despawn();
                growth_writer.send(GrowthEvent);
                food_spawn_writer.send(FoodSpawnEvent);
            }
        }
    }
}

pub fn growth(
    commands: Commands,
    last_tail_position: Res<LastTailPosition>,
    mut segments: ResMut<SnakeSegments>,
    mut growth_reader: EventReader<GrowthEvent>,
) {
    if growth_reader.iter().next().is_some() {
        segments.push(create_segment(commands, last_tail_position.0.unwrap()));
    }
}
