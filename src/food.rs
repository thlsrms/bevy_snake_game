use crate::{
    snake::SnakeSegments,
    transform::{Position, Size},
    ARENA_HEIGHT, ARENA_WIDTH,
};
use bevy::prelude::*;
use rand::prelude::random;

#[derive(Component)]
pub struct Food;
const FOOD_COLOR: Color = Color::rgb(1.0, 0.0, 1.0);
pub struct FoodSpawnEvent;

pub fn spawner(
    mut commands: Commands,
    snake_segments: Res<SnakeSegments>,
    mut food_spawn_reader: EventReader<FoodSpawnEvent>,
    positions: Query<&Position>,
) {
    if food_spawn_reader.iter().next().is_some() {
        let mut new_pos = get_random_position();
        let mut position_found = false;

        let segment_positions = snake_segments
            .iter()
            .map(|e| *positions.get(*e).unwrap())
            .collect::<Vec<Position>>();

        while !position_found {
            if segment_positions
                .iter()
                .any(|pos| pos.x == new_pos.x && pos.y == new_pos.y)
            {
                new_pos = get_random_position()
            } else {
                position_found = true;
            }
        }

        commands
            .spawn(SpriteBundle {
                sprite: Sprite {
                    color: FOOD_COLOR,
                    ..default()
                },
                ..default()
            })
            .insert(Food)
            .insert(new_pos)
            .insert(Size::square(0.8));
    }
}

fn get_random_position() -> Position {
    Position {
        x: (random::<f32>() * ARENA_WIDTH as f32) as i32,
        y: (random::<f32>() * ARENA_HEIGHT as f32) as i32,
    }
}
