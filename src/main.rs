use bevy::{prelude::*, time::FixedTimestep};
use bevy_snake::*;

fn main() {
    App::new()
        .insert_resource(ClearColor(Color::rgb(0.04, 0.04, 0.04)))
        .insert_resource(snake::SnakeSegments::default())
        .insert_resource(snake::LastTailPosition::default())
        .add_event::<snake::GrowthEvent>()
        .add_event::<food::FoodSpawnEvent>()
        .add_event::<GameOverEvent>()
        .add_startup_system(setup_camera)
        .add_startup_system(snake::spawn)
        .add_system_set(
            SystemSet::new()
                .with_run_criteria(FixedTimestep::step(0.150))
                .with_system(snake::movement)
                .with_system(snake::eating.after(snake::movement))
                .with_system(snake::growth.after(snake::eating)),
        )
        // We ensure that we get user input before we move the snake
        .add_system(snake::movement_input.before(snake::movement))
        .add_system(game_over.after(snake::movement))
        .add_system_set_to_stage(
            CoreStage::PostUpdate,
            SystemSet::new()
                .with_system(transform::position_translation)
                .with_system(transform::size_scaling),
        )
        // Periodically spawn food every 1 sec
        /*.add_system_set(
            SystemSet::new()
                .with_run_criteria(FixedTimestep::step(1.0))
                .with_system(food::spawner),
        )*/
        .add_system(food::spawner)
        .add_plugins(DefaultPlugins.set(WindowPlugin {
            window: WindowDescriptor {
                title: "Snake!".to_string(),
                width: 360.0,
                height: 360.0,
                ..default()
            },
            ..default()
        }))
        .run();
}
