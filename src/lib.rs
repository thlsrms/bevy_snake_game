pub mod food;
pub mod snake;
pub mod transform;
use bevy::prelude::{
    Camera2dBundle, Commands, Entity, EventReader, EventWriter, Query, ResMut, With,
};
use food::{Food, FoodSpawnEvent};
use snake::{SnakeSegment, SnakeSegments};

pub const ARENA_WIDTH: u32 = 20;
pub const ARENA_HEIGHT: u32 = 20;
pub struct GameOverEvent;

pub fn setup_camera(mut commands: Commands) {
    commands.spawn(Camera2dBundle::default());
}

pub fn game_over(
    mut commands: Commands,
    mut reader: EventReader<GameOverEvent>,
    food_spawn_writer: EventWriter<FoodSpawnEvent>,
    segments_res: ResMut<SnakeSegments>,
    food: Query<Entity, With<Food>>,
    segments: Query<Entity, With<SnakeSegment>>,
) {
    if reader.iter().next().is_some() {
        for ent in food.iter().chain(segments.iter()) {
            commands.entity(ent).despawn();
        }
        snake::spawn(commands, segments_res, food_spawn_writer);
    }
}
